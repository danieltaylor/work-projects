#A PowerShell script, written with PS 4.0
#Adds Active Directory context and responsible IT Staff to .csv of users and machines
#Input .csv file with 'Machine_Name', 'IPV4, IPV6', and 'Last_User' columns
#Author: Dan Taylor
#28 Sep 2017

Import-Module ActiveDirectory

########################### Methods (Modules) ##################################################

#Tries to check the names in AD, first asks Check-AD if the name is in AD.
New-Module -scriptBlock {
        function Look-Up ($thing) {

           if(Check-AD $thing -eq $true){
                $lastUser = Get-ADUser $thing -Properties CanonicalName
                $userOU = ($lastUser.DistinguishedName -split ",",6)[1]
           }else{
                $userOU = $thing
        }             
 $userOU
};Export-ModuleMember -function Look-Up}
         
#Verifies that user is in AD. If the name isn't found in AD, returns false. 
New-Module -scriptBlock {
    function Check-AD ($nameToCheck){
        $User = Get-ADUser -LDAPFilter "(sAMAccountName=$nameToCheck)"
        If ($User -eq $null){
            $false
        }
        else {
            $true
        }
    }
}

New-Module -ScriptBlock {
    function Assign-Context ($OU) {
        if($OU.StartsWith("OU=")){
            $OU = $OU.TrimStart("OU=")
            $contexts.Get_Item($OU)
        }
    }
}



#################### Reading in data #########################################################
$originalCSVpath = Read-Host 'Enter the absolute path to .csv file containing the new hosts'
$hostCSV = Import-Csv $originalCSVpath

$hostArray = @() # Create an array for host objects

#Initialize each objHost with values 
ForEach($hostInCsv in $hostCSV){
    $objHost = New-Object System.Object
    $objHost | Add-Member -type NoteProperty -name HostName -value $hostInCsv.Machine_Name
    $objHost | Add-Member -type NoteProperty -name IPv4IPv6 -value $hostInCsv.'IPV4, IPV6'
    $objHost | Add-Member -type NoteProperty -name Last_User -value $hostInCsv.Last_User
    $hostArray += $objHost
}

#Creates hash table for IT Admin contexts, from separate provided .csv 
$ITAminCSVpath = Read-Host 'Enter the aboslute path to the .csv file containing the IT Admins'
$ITAdminCSV = Import-Csv $ITAminCSVpath
$contexts = @{}

ForEach($line in $ITAdminCSV){ #For each line in the list, put it in a list
    $contexts.Add($line.Context, $line.Support)
}


#Assigns Last_User_Context to each Last_User in the .csv
ForEach($obj in $hostArray){
    
    #If there is no last user, what should we do? Making the default behavior simply making the context ''
    if($obj.Last_User -eq ''){
        $obj | Add-Member -type NoteProperty -name Last_User_Context -value $obj.Last_User
    }else{
        $tempval = Look-Up $obj.Last_User
        $obj | Add-Member -type NoteProperty -name Last_User_Context -value $tempval
    }

    #Assigns support Admin to each object
    if ($obj.Last_User_Context -eq ''){
        $obj | Add-Member -type NoteProperty -name Support -value ''
    }else{
        $tempval = Assign-Context $obj.Last_User_Context
        $obj | Add-Member -type NoteProperty -name Support -value $tempval
    }
}




######################## CSV Exporting ##################################################


#Now create a new csv, with the original name appending _withContexts
#$originalCSVpath = 'C:\Users\dtaylor\Documents\crazyCSV.csv'
$afterADCsvpath = $originalCSVpath.TrimEnd('.csv')
$afterADCsvpath = $afterADCsvpath.Insert($afterADCsvpath.Length, '_withContextsAndSupport.csv')


#Imports original CSV and adds a column for Last_User_Context
$afterADCsv = Import-Csv $originalCSVpath
$afterADCsv | Export-Csv $afterADCsvpath -NoTypeInformation
$afterADcsv | Add-Member -MemberType NoteProperty -Name 'Last_User_Context' -Value $null
$afterADCsv | Add-Member -MemberType NoteProperty -Name 'Support' -Value $null

#Creates a new CSV
$i = 0
ForEach($line in $afterADCsv){
 
    $line.Last_User_Context = $hostArray[$i].Last_User_Context
    $line.Support = $hostArray[$i].Support
    $i = $i + 1
}

#Exports new CSV
$afterADCsv | Export-Csv $afterADCsvpath -NoTypeInformation