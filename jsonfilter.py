# Dan Taylor
# Filter alert events. Flexible for any EDR / cloud solution

# native
import re

class JsonFilter:
    def __init__(self, name, conditions):
        self.name = name
        self.conditions = conditions

    def apply(self, obj):
        """
        Returns False if all of the conditions match given obj

        :obj: dict, json like object
        """

        for c in self.conditions:
            if not apply_condition(c, obj):
                return True

        return False

def apply_condition(condition, obj):
    """
    Returns true if condition matches the object

    :condition: dict
    :obj: dict
    """
    o = condition.get('operator')
    k = condition.get('key')
    v = condition.get('value')

    if o == 'EQUALS':
        return cond_equals(k, v, obj)
    elif o == 'REGEX':
        return cond_regex(k, v, obj)
    elif o == 'REGEX_ONE_MATCH':
        return cond_regex_count_is(k, v, obj, 1)
    elif o == 'REGEX_ZERO_MATCH':
        return cond_regex_count_is(k, v, obj, 0)
    elif o == 'LENGTH_LESS_THAN':
        return cond_length_less_than(k, v, obj)

def get_value(p, obj):
    """
    Recursive function. Returns value given key path separated by |
    :p: string, like 'indicator|_id'
    :obj: dict
    """
    if type(p) == str:
        p = p.split('|')
    elif len(p) == 0:
        return

    v = obj.get(p[0])
    if type(v) == str or type(v) == int:
        return v
    
    dicts = [i for i in obj.values() if type(i) == dict]
    for d in dicts:
        v = get_value(p[1:], d)
        if v:
            return v

def cond_equals(k, v, obj):
    """
    Returns True if key in obj equals v
    """
    val = get_value(k, obj)
    return v == val

def cond_regex(k, v, obj):
    """
    Returns true if regex v matches obj's value at k
    """
    val = get_value(k, obj)
    if not val:
        return

    matches = re.search(v, val)
    if matches:
        return True

def cond_regex_count_is(k, v, obj, count):
    """
    Returns True if regex matches exact number of times
    """
    val = get_value(k, obj)
    if not val:
        return
    
    if len(re.findall(v, val)) == count:
        return True 

def cond_length_less_than(k, v, obj):
    """
    Returns true if length of obj[k] < v
    """
    val = get_value(k, obj)
    if not val:
        return
    return (len(val) < v)

    
'''
# Example filters
filters = [
    {
        'name': 'Ticket-1234_blacklist_dtaylor',
        'conditions': [
            {
                'key': 'event_values|processEvent/username',
                'operator': 'EQUALS',
                'value': 'DOMAIN\\dtaylor'
            }
        ]
    },
    {
        'name': 'Ticket-5342_allow_by_region',
        'conditions': [
            {
                'key': 'indicator|category',
                'operator': 'EQUALS',
                'value': 'Custom'
            },
            {
                'key': 'controller',
                'operator': 'REGEX',
                'value': '(one|other)'
            }
        ]
    }
]
'''